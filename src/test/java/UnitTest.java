package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class UnitTest {
    
    public UnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
     public void testWinRow1(){
         String[][] table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
         String Player = "O";
         int row = 1;
         int col = 1;
         boolean result = Lab3.checkWin(table, Player, row,col);
         assertEquals(true, result);
     }
     
     @Test
     public void testWinRow2(){
         String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
         String Player = "O";
         int row = 2;
         int col = 2;
         boolean result = Lab3.checkWin(table, Player, row,col);
         assertEquals(true, result);
     }
     
     @Test
     public void testWinRow3(){
         String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
         String Player = "O";
         int row = 3;
         int col = 1;
         boolean result = Lab3.checkWin(table, Player, row,col);
         assertEquals(true, result);
     }
     
     @Test
     public void testWincol1(){
         String[][] table = {{"O","-","-"},{"O","-","-"},{"O","-","-"}};
         String Player = "O";
         int row = 1;
         int col = 1;
         boolean result = Lab3.checkWin(table, Player, row,col);
         assertEquals(true, result);
     }
     
     @Test
     public void testWincol2(){
         String[][] table = {{"-","O","-"},{"-","O","-"},{"-","O","-"}};
         String Player = "O";
         int row = 2;
         int col = 2;
         boolean result = Lab3.checkWin(table, Player, row,col);
         assertEquals(true, result);
     }
     
     @Test
     public void testWincol3(){
         String[][] table = {{"-","-","O"},{"-","-","O"},{"-","-","O"}};
         String Player = "O";
         int row = 1;
         int col = 3;
         boolean result = Lab3.checkWin(table, Player, row,col);
         assertEquals(true, result);
     }
     @Test
     public void testWinDia1(){
         String[][] table = {{"O","-","-"},{"-","O","-"},{"-","-","O"}};
         String Player = "O";
         int row = 1;
         int col = 3;
         boolean result = Lab3.checkWin(table, Player, row,col);
         assertEquals(true, result);
     }
     
     @Test
     public void testWinDia2(){
         String[][] table = {{"-","-","O"},{"-","O","-"},{"O","-","-"}};
         String Player = "O";
         int row = 1;
         int col = 3;
         boolean result = Lab3.checkWin(table, Player, row,col);
         assertEquals(true, result);
     }
     @Test
     public void testDraw(){
         String[][] table = {{"O","X","O"},{"X","O","O"},{"X","O","X"}};
         boolean result = Lab3.checkDraw(table);
         assertEquals(true, result);
     }

    



    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
