/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;
import java.util.Scanner;
/**
 *
 * @author User
 */
public class Lab3 {
    static Scanner kb =new Scanner(System.in);
    static String player = "X";
    static int row,col;
    static String [][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};

    public static boolean checkWin(String[][] table,String Player,int row,int col){
        if(checkRow(table,Player,row)){
            return true;
        }if(checkCol(table,Player,col)){
            return true;
        }if(checkDia(table,Player)){
            return true;
        }
        return false;
    }
   public static boolean checkRow(String[][] table, String Player, int row) {
        for(int i=0;i<3;i++){
            if(!table[row-1][i].equals(Player)){
                return false;       
            }   
        }
        return true;
    }
   public static boolean checkCol(String[][] table, String Player, int col) {
        for(int i=0;i<3;i++){
            if(!table[i][col-1].equals(Player)){
                return false;       
            }   
        }
        return true;
    }
   public static boolean checkDia(String[][] table,String player){
         for(int i=0,j=2;i<3;i++,j--){
             if(!table[i][i].equals(player)&&!table[i][j].equals(player)){
                 return false;
             }
         }
        return true;
    }
    public static boolean checkDraw(String[][] table){
       for(int i=0;i<2;i++){
           for(int j=0;j<2;j++){
               if(table[i][j].equals("-")){
                   return false;
               }
           }
       }
       return true;
   }
    static boolean InputContinue() {
        System.out.print("Please input continue or exit : ");
        String go = kb.next();
        if (go.equals("continue")) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    table[i][j] = "-";
                }
            }
            player = "O";
            return false;
        }
     return true;
    }
    static void ShowWin(){
        printTable();
        System.out.println(player+" win");
    }
    static void printTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println();
        }
    }
    static void printWelcome(){
        System.out.println("Welcome to XO");
    }
    static void ChangePlayer(){
        if(player.equals("X")){
            player="O";
        }else{
            player="X";
        }
    }
    static void printTurn(){
        System.out.println(player+" turn");
    }
    static void InputRowCol(){
        System.out.print("Please input row,col : ");
        row = kb.nextInt();
        col = kb.nextInt();
        if(table[row-1][col-1].equals("-")){
            table[row-1][col-1]=player;
            
        }else{
            System.out.println("cann't move");
            InputRowCol();
        }
    }
    public static void main(String[] args) {
    printWelcome();
        while (true) {
            printTable();
            printTurn();
            InputRowCol();
            if (checkWin(table,player,row,col)){
                ShowWin();
                if(InputContinue()){
                    break;
                }
            }
            if(checkDraw(table)){
                printTable();
                System.out.println("Tie on one win");
                if(InputContinue()){
                    break;
                }
            }
            ChangePlayer();
        }
    }

}
